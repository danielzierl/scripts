#!/bin/bash

OUTPUT=$(nmcli con show --active | rg -i vpn | sed 's/\s/\t/g' | awk -F "\t" '{print $1}')

if [ -z "$OUTPUT" ]
then
    OUTPUT="No VPN"
fi
echo $OUTPUT
