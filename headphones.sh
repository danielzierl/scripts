MAC=$(echo "AC:80:0A:9E:51:C7")
STORE_LOC=~/headphones.meta
STORE=$(cat ~/headphones.meta)
turn_head_on(){
    bluetoothctl connect $MAC
}

turn_head_off(){
    bluetoothctl disconnect $MAC
}
headphones_info(){
    OUTPUT=$(bluetoothctl info $MAC | grep "Connected: \(.*\)" | sed 's/Connected: //' | sed 's/\t//g' )
    BATTERY=$(bluetoothctl info $MAC | grep -P "Battery Percentage: .*\\(\d*\)" | sed 's/Battery Percentage: //' \
        | sed 's/\t//g' | grep -Po "\(\d{1,3}\)" | head -n 1 | sed 's/(/ /g' | sed 's/)//g' )
    BATTERY=$(echo "$BATTERY%" )
    if [ "$BATTERY" = "%" ]
    then
        echo " : $OUTPUT" > "$STORE_LOC"
    else
        echo " Battery:$BATTERY" > "$STORE_LOC"
    fi

}

if [ "$1" = "info" ]
then
    headphones_info
elif [ "$1" = "on" ]
then
    hyprctl notify 5 2000 0 "Connecting headphones..."
    turn_head_on

elif [ "$1" = "off" ]
then
    hyprctl notify 1 2000 0 "Disconnecting headphones..."
    turn_head_off
fi 
