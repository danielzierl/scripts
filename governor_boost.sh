#!/bin/bash

BOOST_STATUS=$(cat /sys/devices/system/cpu/cpufreq/boost)
GOVERNOR=$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor | sed -e "s/\b\(.\)/\u\1/g")
if [ $BOOST_STATUS == "1" ]
then
    BOOST_STATUS="Boost"
else
    BOOST_STATUS="No Boost"
fi

echo $BOOST_STATUS $GOVERNOR
