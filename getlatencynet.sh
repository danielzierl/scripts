
#!/bin/bash
time_limit=0.4

ping -c 1 1.1.1.1 | sed -n '2 p' | awk '{print $7}' | awk -F '=' '{print $2}' &
command_pid=$!

timeout $time_limit tail -f /dev/null

# Check if the command finished within the time limit
if kill -0 $command_pid 2> /dev/null; then
    # Command is still running, kill it
    kill $command_pid
    echo "NET TIMEOUT"
else
    echo "SUCCESS"
fi
