import os
import shutil
from time import sleep
from typing import Set

import numpy

path = "/home/danikpapas/Downloads"


def loopThroughFiles():
    files = os.listdir(path)
    for file in files:
        filename, extension = os.path.splitext(file)
        extension = extension[1:]
        if os.path.isdir(path + "/" + file):
            continue

        if extension.__eq__(''):
            if not os.path.exists(path + "/" + "noExtension"):
                os.mkdir(path + "/" + "noExtension")
            shutil.move(path + "/" + file, path + "/" + "noExtension")
        else:
            if not os.path.exists(path + "/" + extension):
                os.mkdir(path + "/" + extension)
            shutil.move(path + "/" + file, path + "/" + extension)


morse_dict = {'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.', 'F': '..-.', 'G': '--.', 'H': '....',
              'I': '..', 'J': '.---', 'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---', 'P': '.--.',
              'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-', 'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-',
              'Y': '-.--', 'Z': '--..', '0': '-----', '1': '.----', '2': '..---', '3': '...--', '4': '....-',
              '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.', ' ': ""}


def decode_morse(morse_code):
    print([morse if morse.__eq__('') else ' xx' for morse in [morse_code.split(" ")]   ])
    return "".join(
        [[key for (key, value) in morse_dict.items() if value == morse][0] for morse in morse_code.split(" ")])


def main():
    while True:
        loopThroughFiles()
        sleep(10)


if __name__ == "__main__":
    print(decode_morse('.... . -.--   .--- ..- -.. .'))
    main()
