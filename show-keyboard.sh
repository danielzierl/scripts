#!/bin/bash

switch_toggle () {
  STORE=$(cat ~/switch-keyboard_no_emoji.meta)
  if [ $STORE == 'dvorak' ]
  then
    LANG=$(echo 'en')
    hyprctl keyword input:kb_layout "us" > /dev/null
  elif [ $STORE == 'en' ]
  then
    LANG=$(echo 'cz')
    hyprctl keyword input:kb_layout "cz(qwerty)" > /dev/null 
  else
    LANG=$(echo 'dvorak')
    hyprctl keyword input:kb_layout "us(dvorak)" > /dev/null 
  fi
  set_keyb $LANG
    
  send_notification $LANG
}

switch_set () {
    set_keyb $1
    # echo "$1" > ~/switch-keyboard.meta
    if [ $1 == 'dvorak' ]
    then
        hyprctl keyword input:kb_layout "us(dvorak)" > /dev/null 
    elif [ $1 == 'cz' ]
    then
        hyprctl keyword input:kb_layout "cz(qwerty)" > /dev/null 
    else
        hyprctl keyword input:kb_layout "us" > /dev/null 
    fi

    send_notification $1
}
set_keyb () {
    echo "󰌌  $1" > ~/switch-keyboard.meta
    echo "$1" > ~/switch-keyboard_no_emoji.meta
}
send_notification () {
    notify-send --expire-time=700 "Keyboard switched to 󰌌 $1"
}

if [ "$1" = 'toggle' ]
then
  switch_toggle
else
  switch_set $1
fi

