#!/bin/bash
clipboard=$(wl-paste)
curl --request POST \
  --url http://localhost:8080/hey \
  --header 'Content-Type: application/json' \
  --data "{
	\"message\": \"$clipboard\"
}"
notify-send --expire-time=700 "Clipboard saved: $clipboard"
