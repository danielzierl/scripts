#!/bin/bash
time_limit=0.4

dig google.com | rg time | awk '{print $4}' &    
command_pid=$!

timeout $time_limit tail -f /dev/null

# Check if the command finished within the time limit
if kill -0 $command_pid 2> /dev/null; then
    # Command is still running, kill it
    kill $command_pid
    echo "DNS TIMEOUT"
else
    echo "SUCCESS"
fi
