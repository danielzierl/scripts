mic_listen_on() {
    pactl load-module module-loopback latency_msec=0
}

mic_listen_off() {
    pactl unload-module module-loopback
}

if pactl list modules | grep -q "module-loopback"; then
    mic_listen_off
else
    mic_listen_on
fi
